package br.com.productmanager.resource;

import br.com.productmanager.entity.Image;
import br.com.productmanager.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by rodmafra on 2017-11-22.
 */
@RestController
public class ImageResource {

    private ImageRepository imageRepository;

    @Autowired
    public ImageResource(ImageRepository productRepository) {
        this.imageRepository = productRepository;
    }

    @RequestMapping(value = "/image", method = RequestMethod.POST)
    public void addImage(@RequestBody Image image) {
        this.imageRepository.save(image);
    }

    @RequestMapping(value = "/image/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Long id) {
        this.imageRepository.delete(id);
    }

    @RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
    public ResponseEntity getImage(@PathVariable Long id) {
        ResponseEntity responseEntity;
        final Image image = this.imageRepository.findOne(id);

        if (image != null) {
            responseEntity = new ResponseEntity<Image>(image, HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity<Image>(HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }


    @RequestMapping(value = "/image/{id}", method = RequestMethod.PUT)
    public ResponseEntity<HttpStatus> updateImage(@PathVariable Long id, @RequestBody Image image) {

        final Image one = this.imageRepository.findOne(id);

        if (one != null) {
            image.setId(one.getId());
            this.imageRepository.save(image);
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } else {
            return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
        }

    }

    @ResponseBody
    @RequestMapping(value = "/images", method = RequestMethod.GET)
    public ResponseEntity list() {
        ResponseEntity responseEntity;
        final Iterable<Image> images = this.imageRepository.findAll();

        if (images.iterator().hasNext()) {
            responseEntity = new ResponseEntity<Iterable<Image>>(images, HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity<Iterable<Image>>(HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }


    @ResponseBody
    @RequestMapping(value = "/images/product", method = RequestMethod.GET)
    public ResponseEntity listByProductName(@RequestParam String productName) {

        ResponseEntity responseEntity;
        final Iterable<Image> images = this.imageRepository.getProductImageList(productName);

        if (images.iterator().hasNext()) {
            responseEntity = new ResponseEntity<Iterable<Image>>(images, HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity<Iterable<Image>>(HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }

}
