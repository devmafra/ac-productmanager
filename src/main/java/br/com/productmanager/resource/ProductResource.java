package br.com.productmanager.resource;

import br.com.productmanager.entity.Product;
import br.com.productmanager.exception.ChildAlreadyHasParentException;
import br.com.productmanager.exception.DuplicatedProductException;
import br.com.productmanager.repository.ProductRepository;
import br.com.productmanager.service.ProductServiceImpl;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by rodmafra on 2017-11-22.
 */
@RestController
public class ProductResource {

    private ProductRepository productRepository;
    private ProductServiceImpl productService;

    @Autowired
    public ProductResource(ProductRepository productRepository, ProductServiceImpl productService) {
        this.productRepository = productRepository;
        this.productService = productService;
    }

    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public void addProduct(@RequestBody Product product) throws ChildAlreadyHasParentException, DuplicatedProductException {
        this.productService.addProduct(product);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Long id) {
        this.productRepository.delete(id);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
    public ResponseEntity<HttpStatus> updateProduct(@PathVariable Long id, @RequestBody Product product) {

        final Product one = this.productRepository.findOne(id);

        if (one != null) {
            product.setId(one.getId());
            this.productService.updateProduct(product);
            return new ResponseEntity<HttpStatus>(HttpStatus.OK);
        } else {
            return new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND);
        }

    }

    @ResponseBody
    @RequestMapping(value = "/products", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity list(@RequestHeader(value="Object-type") String type) throws JsonProcessingException {

        final Iterable<Product> products = this.productRepository.findAll();
        ResponseEntity responseEntity;
        if (products.iterator().hasNext()) {
            responseEntity = new ResponseEntity<String>(configureMapper(type).writeValueAsString(products), HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }

        return responseEntity;

    }

    @ResponseBody
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity listOne(@RequestHeader(value="Object-type") String type, @PathVariable Long id) throws JsonProcessingException {

        ResponseEntity responseEntity;
        final Product product = this.productRepository.findOne(id);

        if (product != null) {
            responseEntity = new ResponseEntity<String>(configureMapper(type).writeValueAsString(product), HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity<String>(HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }

    @ResponseBody
    @RequestMapping(value = "/product/children", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity listChild(@RequestParam String name) throws JsonProcessingException {
        ResponseEntity responseEntity;
        final List<Product> productChildList = this.productRepository.getProductChildList(name);

        if (CollectionUtils.isEmpty(productChildList)) {
            responseEntity = new ResponseEntity<List<Product>>(HttpStatus.NOT_FOUND);
        } else {
            responseEntity = new ResponseEntity<List<Product>>(productChildList, HttpStatus.OK);
        }

        return responseEntity;
    }


    private ObjectMapper configureMapper(String objectType) {

        ObjectMapper mapper = new ObjectMapper();
        if (objectType.equals("plain")) {
            mapper.addMixIn(List.class, IgnoreType.class);
        }
        return mapper;
    }

    @JsonIgnoreType
    private abstract class IgnoreType {}

}
