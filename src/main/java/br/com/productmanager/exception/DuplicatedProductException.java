package br.com.productmanager.exception;

/**
 * Created by rodmafra on 2017-11-22.
 */
public class DuplicatedProductException extends Exception {

    public DuplicatedProductException(String message) {
        super(message);
    }

}
