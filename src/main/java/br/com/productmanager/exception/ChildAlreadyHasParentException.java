package br.com.productmanager.exception;

/**
 * Created by rodmafra on 2017-11-22.
 */
public class ChildAlreadyHasParentException extends Exception {

    public ChildAlreadyHasParentException(String message) {
        super(message);
    }

}
