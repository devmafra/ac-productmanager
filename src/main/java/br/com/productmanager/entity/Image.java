package br.com.productmanager.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by rodmafra on 2017-11-21.
 */
@Entity
@Data
public class Image {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column
    String type;

}
