package br.com.productmanager.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rodmafra on 2017-11-21.
 */
@Entity
@Data
public class Product {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String name;

    @Column
    private String description;

    @OneToMany (cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private List<Image> images = new ArrayList<Image>();

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="parent_product_id")
    private Product parent;

    @JsonManagedReference
    @OneToMany(mappedBy="parent", cascade = CascadeType.ALL)
    public List<Product> subProducts = new ArrayList<Product>();

    public void addImage(Image image) {
        this.images.add(image);
    }

    public void addSubProduct(Product product) {
        this.subProducts.add(product);
    }

}
