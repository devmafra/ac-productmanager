package br.com.productmanager.repository;

import br.com.productmanager.entity.Product;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by rodmafra on 2017-11-22.
 */
@RepositoryRestResource(exported = false)
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query("from Product where Name = :productName")
    Product getProductByName(@Param("productName") String productName);

    @Query("select distinct p.subProducts from Product p join p.subProducts sub where p.name = :productName")
    List<Product> getProductChildList(@Param("productName") String productName);

    @Transactional
    @Modifying
    @Query("delete from Product p where p.parent.id = :parentId")
    void removeChildList(@Param("parentId") Long parentId);
}
