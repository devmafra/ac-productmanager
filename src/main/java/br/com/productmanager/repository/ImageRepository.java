package br.com.productmanager.repository;

import br.com.productmanager.entity.Image;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by rodmafra on 2017-11-22.
 */
@RepositoryRestResource(exported = false)
public interface ImageRepository extends CrudRepository<Image, Long> {


    @Query("select distinct p.images from Product p join p.images img where p.name = :productName")
    List<Image> getProductImageList(@Param("productName") String productName);


}
