package br.com.productmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by rodmafra on 2017-11-22.
 */
@SpringBootApplication
@EnableTransactionManagement
@EnableEntityLinks
public class Application {

    public static void main(String ... args) {
        SpringApplication.run(Application.class);
    }

}
