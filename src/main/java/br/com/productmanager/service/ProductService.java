package br.com.productmanager.service;

import br.com.productmanager.entity.Product;
import br.com.productmanager.exception.ChildAlreadyHasParentException;
import br.com.productmanager.exception.DuplicatedProductException;

/**
 * Created by rodmafra on 2017-11-24.
 *
 * Service with business rules regarding {@link Product}
 *
 */
public interface ProductService {

    void addProduct(Product product) throws ChildAlreadyHasParentException, DuplicatedProductException;

    void updateProduct(Product product);


}
