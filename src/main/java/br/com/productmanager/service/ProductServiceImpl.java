package br.com.productmanager.service;

import br.com.productmanager.entity.Product;
import br.com.productmanager.exception.ChildAlreadyHasParentException;
import br.com.productmanager.exception.DuplicatedProductException;
import br.com.productmanager.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by rodmafra on 2017-11-22.
 */
@Service
public class ProductServiceImpl {


    private ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void updateProduct(Product product) {
        this.productRepository.save(product);
    }

    @Transactional
    public void addProduct(Product product) throws ChildAlreadyHasParentException, DuplicatedProductException {

        if (this.productRepository.getProductByName(product.getName()) != null) {
            throw new DuplicatedProductException("Product with name "+product.getName()+"already exists in the database");
        }

        for (Product pd : product.getSubProducts()) {
            final Product pdDb = this.productRepository.getProductByName(pd.getName());
            if (pdDb != null) {
                if (pdDb.getParent() != null && ( pdDb.getParent().getName().equals(product.getName())) ) {
                    throw new ChildAlreadyHasParentException("Child product already has a parent.");
                }
            }
        }
        this.productRepository.save(product);
    }

}
