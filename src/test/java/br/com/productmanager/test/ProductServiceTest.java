package br.com.productmanager.test;

import br.com.productmanager.entity.Image;
import br.com.productmanager.entity.Product;
import br.com.productmanager.exception.ChildAlreadyHasParentException;
import br.com.productmanager.exception.DuplicatedProductException;
import br.com.productmanager.repository.ProductRepository;
import br.com.productmanager.service.ProductServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by rodmafra on 2017-11-24.
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {


    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;

    private Product product;


    @Before
    public void setUp() {

        productService = new ProductServiceImpl(productRepository);
        Image image1 = new Image();
        image1.setType("Type 1");
        Image image2 = new Image();
        image1.setType("Type 2");
        Product product1 = new Product();
        product1.setName("Product 1 sub");
        product1.setDescription("Product 1 sub test");
        Product product2 = new Product();
        product2.setName("Product 2 sub");
        product2.setDescription("Product 2 sub test");


        product = new Product();
        product.setName("Product 1");
        product.setDescription("Product 1 test");
        product.addImage(image1);
        product.addImage(image2);
        product.addSubProduct(product1);
        product.addSubProduct(product2);
    }

    @Test
    public void insert_unique_product() throws ChildAlreadyHasParentException, DuplicatedProductException {

        product.setId(1l);
        Mockito.doReturn(product).when(productRepository).save(product);
        Mockito.when(productRepository.getProductByName("Product 1")).thenReturn(null);
        productService.addProduct(product);

        Assert.assertTrue(product.getId().longValue() == 1l);
    }

    @Test( expected = DuplicatedProductException.class)
    public void insert_duplicated_product() throws ChildAlreadyHasParentException, DuplicatedProductException {
        product.setId(1l);
        Mockito.doReturn(product).when(productRepository).save(product);
        Mockito.when(productRepository.getProductByName(Mockito.anyString())).thenReturn(product);
        productService.addProduct(product);
    }

    @Test( expected = ChildAlreadyHasParentException.class)
    public void insert_product_child_with_existing_parent() throws ChildAlreadyHasParentException, DuplicatedProductException {
        Product product1 = new Product();
        product1.setName("Product 1 sub");
        product1.setDescription("Product 1 sub test");
        product1.setParent(product);
        product.addSubProduct(product1);

        product.setId(1l);
        Mockito.doReturn(product).when(productRepository).save(product);
        Mockito.when(productRepository.getProductByName("Product 1")).thenReturn(null);
        Mockito.when(productRepository.getProductByName("Product 1 sub")).thenReturn(product1);
        productService.addProduct(product);
    }




}
