# AC Product Manager #

Simple API used to manage a Product and it's images.


It's main features are:

*  Create, update and delete products
*  Create, update and delete images
*  Get all products excluding relationships (child products, images) 
*  Get all products including specified relationships (child product and/or images) 
*  Same as 3 using specific product identity 
*  Same as 4 using specific product identity 
*  Get set of child products for specific product 
*  Get set of images for specific product

* Some of the features like updating a Product have some business rules and unit
* tests to validate them. One of them is the rule of inserting more than one Product with
* the same name. These rules were not described but are implemented in order to add some business testing.


## Product manager contains 1 module ##

* productmanager (connects with an in memory database (H2) and contains the all the CRUD features)

### Building ###

```
ac-productmanager> $ mvn clean package
```

### Testing ###

```
ac-productmanager> $ mvn test
```


### Running ###
Pre-requisites: jdk-8 installed, maven installed and added to the PATH of your OS

Bellow's the command line to run the app
```
ac-productmanager> $ mvn spring-boot:run
```

In order to test the API the chrome extension postman (https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop)
can be used.

The file "Avenue Product Challenge.postman_collection.json" file can be imported into the extension for sample usage and testing purposes. It contains the authentication mechanism samples as well (both valid and invalid tokens). The token has to be sent through the header as the postman example shows.

* The header parameter Object-type=plain is used to format the output of the Product List/GET operation. In order to return the full object
* this parameter need to have another value or be removed.


Note: This project uses lombok (https://projectlombok.org/) for code generation avoiding boilerplate code.
